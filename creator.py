#!/usr/bin/env python3
# -*- coding: utf-8 -*-
''' 
* Copyright 2022 Leonardo de Araújo Lima 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program. If not, see http://www.gnu.org/licenses/

'''

from dStorage.core import dStorage
import os, shutil

class creator():
    def __init__(self):
            self.info = "pyCreator, aplicativo para construção de softwares."
            self.desc = "Este aplicativo utiliza a metaclasse dStorage para construção de interfaces, CLI, GUI e ambiente de dados."
            self.itype = 0
            self.db = ""
            self.tb = ""
            self.appname = ""
            self.confs = ""
            self.npath=""
            self.debug = 1
          
    def start(self):
        print("tipo de interface: ", self.itype)
        self.tmpi = []
        self.tmpd = []
        print("qual o nome do novo aplicativo?")
        self.appname = input()
        print("qual a descrição do novo aplicativo?")
        self.desc = input()
        print("Adicionando propriedades a nova classe.")
        self.tmpi.append("id")
        self.tmpd.append(0)
        pa = "s"
        while (pa != "n"):
            pa=input("adicionar nova propriedade a classe (s/n) ? ")
            if(pa == "s"):
                t = input("nome da propriedade a ser adicionada ? ")
                if (t):
                    self.tmpi.append(t)

        for a in range(len(self.tmpi) -1 ):
            tmpv = "info-"+ self.tmpi[a]
            self.tmpd.append(tmpv)
                
        print("número de propriedades da classe: ", len(self.tmpi))
        print("lista de propriedades da classe a ser gerada.")
        for i in self.tmpi:
            print(i)
                
        print("nome da base de dados a ser criada? ")
        self.db = input()
        self.tb = input("nome da tabela de dados? ")

    def cadastro(self):
        c = dStorage([], [])
        c.dpath = os.path.expanduser('~/')
        c.table = self.tb
        c.set_database(self.db)
        '''debug'''
        if self.debug == 1:
            c.dbinfo()
 #

    def test(self):
        c = dStorage([],[])
        c = dStorage([], [])
        c.dpath = os.path.expanduser('~/'+self.appname)
        c.table = self.tb
        c.set_database(self.db)
        print("testando cadastro de dados no novo aplicativo...")
        print("tipo de interface", self.itype)
        c.l_pdindex()
        print(c.pindex)
        if self.itype == 1:
            c.cad()
             
        else:
             c.registrar()
             print("deseja visualizar os dados cadastrados no novo aplicativo?")
             nt = input()
             if nt == "s" or nt =="S":
                 c.show()
                 
             t = input("salvar registro? (s/n) ")
             if t == "s":
                 c.savedata()
#
        print("dados armazenados...")

    def consulta(self):
        c = dStorage([], [])
        c.dpath = os.path.expanduser('~/')
        c.table = self.tb
        c.set_database(self.db)
        c.l_pdindex()
        if self.debug == 1:
            print("testando consulta de dados no novo aplicativo...")
        
        print("registros disponíveis\n")
        print(c.litems())
        r = input("registro a ser visualizado? ")
        c.loaddata(r)
        if self.itype == 0:
            c.show()

        else:
            c.display()

    def construct(self):
        t = input("criar aplicativo (s/n)")
        if t == "s":
            print("Construindo aplicativo...\n")
            #
            shutil.copyfile('app-skell.py',(self.npath + "/" + self.appname))
            os.system("chmod +x  "+("~/"+self.appname + "/" + self.appname))
            shutil.copyfile(os.getcwd() + '/img/logo256.png',(self.npath+"/img/logo256.png"))
            shutil.copyfile('LICENSE',(self.npath + "/LICENSE"))
            shutil.copyfile('COPYING',(self.npath + "/COPYING"))
            
            shutil.copyfile('cREADME.md',(self.npath + "/README.md"))
            strcmd = 'sed -i "s/Nova aplicação/"' + self.appname + '"/" ' + self.npath + '/README.md'
            os.system(strcmd)
            strcmd = 'sed -i "s/Descrição da aplicação/"' + self.desc + '"/" ' + self.npath + '/README.md'
            os.system(strcmd)
            print("Aplicativo criado com sucesso!\n")
            print("O aplicativo se encontra em seu diretório pessoal!/n")
            print("testar o novo aplicativo (s/n) ?")
            nt = input()
            if nt == "s" or nt == "S":
                os.system("cd "+self.npath +" && ./"+self.appname)

    def create(self):
        print("criando classe,  banco de dados SQLITE associado com propriedades de dados da nova classe.")
        print("propriedades:", self.tmpi)
        print("valores iniciais:", self.tmpd)
        myClass = dStorage(self.tmpi, self.tmpd)
        print("classe criada com sucesso!")
        print(myClass.pindex)
        print("dStorage version:", myClass.version)
        myClass.dpath = os.path.expanduser("~/") + self.appname + "/"
        myClass.table= self.tb
        myClass.set_database(self.db)
        '''print("banco de dados: "+ self.db +" e tabela: "+ self.tb + " serão criados.")'''
        t = input("realmente construir? (s/n)")
        self.save_cfile()
        myClass.dbinfo()
        myClass.cdBase()
        print("base de dados criada!")
        print("Aplicativo construído.\n")
        print("Aplicativo: " + self.appname)
        
        print("descrição: " + self.desc)
                
    def checkenv(self):
        '''
        print("Terminal device associated with:")
        print("Standard input:", os.ttyname(0))
        print("Standard output:", os.ttyname(1))
        '''
            
    def save_cfile(self):
        self.npath = os.path.expanduser("~/") + self.appname
        print("caminho do novo aplicativo: "+self.npath)
        os.system("mkdir " + self.npath)
        os.system("mkdir "+self.npath + "/img")
        self.confs += "[Aplicativo]"
        self.confs += "\nTB = " + self.tb
        self.confs += "\nDB = " + self.db
        self.confs += "\nName = " + self.appname
        self.confs += "\nDesc = " + self.desc
        
        self.cfh = open((self.npath + '/app-config'),'w')
        self.cfh.write(self.confs)
        self.cfh.close()
    
    
def main():
    term = os.ttyname(0)
    print("terminal: ",term[5:8])
    if term[5:8] == "pts":
        itype = 1
    else:
        itype = 0

    print("tipo de interface:", itype)
    c = creator()
    if c.debug == 1:
        print(c.info)
        print(c.desc)
        
    c.itype = itype
    c.start()
    c.create()
    c.construct()

    
main()

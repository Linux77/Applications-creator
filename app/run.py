#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

''' 
* Copyright 2023 Leonardo de Araújo Lima 
* This program is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* This program is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License 
* along with this program. If not, see http://www.gnu.org/licenses/

'''

from dStorage.core import dStorage
from Creator.core import creator
import os, shutil

def main():
    term = os.ttyname(0)
    print("terminal: ",term[5:8])
    if term[5:8] == "pts":
        itype = 1
    else:
        itype = 0

    print("tipo de interface:", itype)
    c = creator()
    c.itype = itype
    c.start()
    c.create()
    c.construct()



if __name__ == "__main__":
    main()
 

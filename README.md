# Applications creator

![logo](img/logo-s.png "Applications creator")


# **Here a python simple application creator.**

This project is based on [dStorage](https://pypi.org/project/dStorage/) python library.

it's able to show how  simple is the use of this library.

The library, as a study, the goal of the project is turns more readable the data structure to create data components.


On this project I use dStorage library to provide abstraction on GUI and CLI interfaces for small databases.

The structure is very simple and gives an easy usage.

The creator script make a simple questionary when estabilish the creation of main project struture.

The database tables and associations for dStorage child class.

At moment some files are copied to destination application folder.

The new project is created and stored on application name subdirectory.

## TODO.

Now i need learn and write the pacckge structure on the right way, following the rules to devel python applications. at moment this software work, but i think with it is only an outline.

## USAGE.

**required libraries.**

__tkinter.__

__dStorage.__

__sqlite.__


**dependencies.**

to new apps is needed some image python packages.

on debian based systems you can do:

sudo apt install python3-pil python3-pil.imagetk

**New dependencies.**

To enable new resource with voice output, is needes on debian systems some packages.


espeak, mbrola-br1, mbrola-br2, mbrola-br3, mbrola-br4.

**Changes:**

On old version we have also one app skell with espeak support, this allow data output with text to speech resource.

This enable some facilities to some impaired users.

With this skell the output can be choosed to **CLI** and **GUI** with audio output.

running creator some questions related to future application project.

The app-config file gives to program important settings, like database and table names and data object structure.

The structure is very simple.

Once time that these questions are answered thhe application are created.

## Some instructions.

dStorage library by default creates SQLite database on home folder.

With app-config you have the right database and table names, also the new aplication name.

## New **changes:**

Now genetator build an environment more detailed, new options in creator application turn easy add some information about new generated application.

Also I make a review in some errors.

> The structure needed is created on separate directory, for me all failures are in progress to be corrected.

**usage.**

Some images of usage, and others can be found [**here**](doc/USAGE.md)


Creator creates application directory with application inside.

> The most recent version also create and copy to new application dir the necessary files.

> Like the  new created application config file.

## Testing new simple application.

After these tasks, check if new database are created in user home directory.

If all be fine, the new simple application structure is ready.

## Current.
Now the applications creator can generate config file for new applications.

Also generate new application file, based on app-skell.

On old versions, you need make some adjusts, like change the new generated app-config"app name" to app-config.

Also you need make the "app name" new file executable.


_contact_**/contato:**

### Leonardo.

> Apaixonado por tecnologia, professor com graduação em ensino fundamental e médio.

> Desde menino ainda na década de 80 sempre acompanhando evoluções em telecomunicações.

> Radio amador quando jovem, ainda me lembro de antever tecnologias, em nossos ideiais.

> Programador com experiência em linguagens de alto e baixo nível....

> Tive influências de conhecidos e parentes dobre questões de conhcer ambientes na época distantes da maior parte das pessoas.

> ainda me recordo de uma _tia_, programadora de ambientes UNIX, na época de locação de mainframes, me despertou o interesse por me aprofundar em tais estudos.

> Muitos anos depois tive meus primeiros contatos com ambiente BSD's e Linux o que despertou uma paixão adormecida.

> Hoje tenho mais de 20 anos de ensino técnico em uma estrutura prórpia, a qual em minha pequena cidade e região mostrou eficácia, mudando a vida de muitos adolescentees hoje profissionais espealhados por várias localidades.


**fone:** (35) 99120-5702.

> [email](mailto:leonardo@asl-sl.com.br)

> **Linux77.**

### Mônica.

**fone:** (35) 99853-7574.

**email:** [Mônica](mailto:monijucodoro@gmail.com)

[Academia do software livre **Brasil - MG**](http://www.asl-sl.com.br)

[Courses and studies**](http://www.cursos.asl-sl.com.br)

[Um pouco sobre nossa cidade!](http://www.asl-br.com/TMB)

[A short about our City!](http://www.asl-br.com/TMB)

[contact](mailto:feraleomg@gmail.com)


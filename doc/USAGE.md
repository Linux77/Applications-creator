# Aqui temos uma breve apresentação e demonstrativos.

Isso se refere ao Applications creator.

Este desenvolvido em python para acelerar o processo de construção de pequenas aplicações para bancos de dados.

Ele se baseia no uso da bilioteca dStorage, disponível para instalação via pip.

Aqui uma imagem do conteúdo do diretório principal do applicaition creator.

![image](app-dir_files.png)

Os arquivos podem ser modificados e estudados livremente.

Os demais exemplo também tem essa característica.

Fique a vontade para explora e estudar.

Com o applications creatoro automaticamente você constrói a estrutura necessária para sua nova aplicação.

Isso inclúi, arquivos de licença, textos suplementares e outros.

O banco de dados também é criado de forma automática.

Eu particularmente uso ele para ter rascunhos de novos projetos já funcionais.

Aqui temos o aplicativo em funcionamento ou seja o criador de aplicativos.

![image](app_início.png)

Aqui temos a estrutura de diretório para o aplicativo que foi construído.

Ou seja ao criarmos um aplicativo o creator automaticamente constrói  a estrutura de arquivos e diretórios que vao ser usados.

Isso inclui imagens, etc.

![image](child-app_files.png)

Nesse arquivos vocÊ pode estudar, modificar, adequar as suas necessidades.

![image](child_app_main.png)

Aqui a janela principal do seu novo aplicativo, cosntruído com o creator.

Espero que tenha gostado.

Academia do sofwtare livre:

__Leonado de Araújo Lima.__
(**email**(feraleomg@gmail.com)


[**Academia do software livre.**](https://www.asl-sl.com.br)

---

